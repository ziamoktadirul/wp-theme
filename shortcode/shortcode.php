<?php

// promo shortcode
add_shortcode('promo', 'promo_shortcode');

function promo_shortcode($attr, $content){

ob_start(); 

extract(shortcode_atts(array(
	
), $attr));



?>
<div class="clearfix"></div>
<div class="clearfix margin-top5"></div>
 <section class="section_category36">
 <div class="container">
 <div class="row"> 
<?php 
$pro = new WP_query(array(
'post_type' => 'promo',
));

while($pro->have_posts()): $pro->the_post(); ?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
 <div class="feature-box4 text-center">
 <div id="diamond4"><div class="diamond3 color4" ><div class="icon"><i class="fa fa-<?php echo get_post_meta(get_the_id(), '__promoicon__', true); ?>"></i></div></div></div> 
 <h3><?php the_title(); ?></h3>
 <div class="clearfix margin-top6"></div>
 <p><?php echo wp_trim_words(get_the_content(), 100, '' ); ?></p>
 <div class="clearfix margin-top2"></div>
 <a href="<?php the_permalink(); ?>" class="btn btn-default">Read More</a>
 </div>
 </div>
 <?php endwhile; ?>
  
 </div></div>
 </section>	
<?php return ob_get_clean();

}



// portfolio shortcode
add_shortcode('portfolio', 'portfolio_shortcode');

function portfolio_shortcode($attr, $content){

ob_start(); 

extract(shortcode_atts(array(
	'title' => 'Our Portfolio',
	'cont' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis ut ligula quis ultricies. Integer fusce fringilla venenatis vulputate zz.'
), $attr));



?>
<div class="clearfix"></div>

<section class="section_category16 center">
<div class="portfolio">
<div class="sec_title2">
<h1><?php echo $title; ?></h1> 
</div>
<p><?php echo $cont; ?></p>
</div>
<div class="clearfix"></div>

<div class="container">
<div class="row ">
<div class="portfolio-items">
<?php 

$port = new WP_query(array(
	'post_type'=> 'portfolio'

));


while($port->have_posts()): $port->the_post(); ?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
<div class="portfolio-grid">
<a href="#" class="portfolio-content">
<?php the_post_thumbnail('thum'); ?>
<div class="portfolio-hover">
<div class="hover-content"><div>
<h5 class="portfolio-title"><?php the_title(); ?></h5>
<div class="portfolio-categories">
<span><?php the_content(); ?></span>
</div></div></div></div></a></div>
</div>
<?php endwhile; ?>

</div>
<a href="<?php the_content(); ?>" class="btn btn-border white uppercase margin-left-2"><span>View More Projects</span></a>

</div>
</div>
</section>	
<?php return ob_get_clean();

}



// client shortcode
add_shortcode('client', 'client_shortcode');

function client_shortcode($attr, $content){

ob_start(); 

extract(shortcode_atts(array(
	'title' => 'What Our Client Say',
	
), $attr));

?>

<div class="clearfix"></div>

<section class="section_category parallax-section27">
<div class="parallax-overlay">
<div class="container">

<div class="testimonials2 slide-controls-color-4 pink">
<h2 class="text-center uppercase text-white"><?php echo $title; ?></h2>
<div class="margin-top2"></div>
<div class="clearfix"></div>
<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10 text-center">
<div id="owl-demo2" class="owl-carousel ">

<?php

$cli = new WP_query(array(
'post_type'=> 'client',

));

 while($cli->have_posts()): $cli->the_post(); ?>
 <div class="item">
<div class="testimonials4 ">
<div class="image-left">
<div class="client-img">
<div class="img-inner overflow-hidden"><?php the_post_thumbnail(); ?></div>
</div>
</div>
<div class="description">
<p><?php the_content(); ?></p>
</div>
<div class="clearfix"></div>
<h5><?php the_title(); ?><br><span>CEO</span></h5>
</div>
</div>
<?php endwhile; ?>
</div>
  
  </div>
  </div>

</div>

</div>
</div>
</section>
<?php return ob_get_clean();

}








