﻿<?php 

/*
Template Name:service

*/


get_header(); ?> 
  <!--end menu-->
  <div class="clearfix"></div>
  
  <div class="slider">
  <div class="titlebar three">
  <img src="<?php echo get_template_directory_uri(); ?>/images/service-banner2.jpg" class="img-responsive">
  <div class="container">
   <div class="breadcrumb">    
   <h1>Services 3</h1>
<a href="#">Home</a> <i class="fa fa-angle-double-right"></i> <a href="#">Pages</a> <i class="fa fa-angle-double-right"></i> Services
</div></div></div></div>
   
 <!--end section-->
  <div class="clearfix"></div>


<section class="section_category41 parallax-section21 service">
<div class="container">
<div class="row" data-anim-type="bounce-in-right" data-anim-delay="100">
 <div class="col-lg-12  center">
 <h2 class="uppercase">Our Services</h2>
 <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit Suspendisse et<br>
justo Praesent mattis commodo augue .</p>
 </div></div>

 <div class="margin-top3"></div>
 <div class="clearfix"></div> 

<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="feature-box9">
<div id="hexagon"><div id="hexagon2"><div class="icon"><i class="fa fa-files-o"></i></div></div></div>
<div class="text-box-left">
<h3>Layered PSD Files</h3>
<p>Lorem ipsum dolor sit amet consectetuer<br>
elit et sit amet justo Suspendisse justo.</p>
</div></div></div>

<div class="col-lg-6 col-md-6 col-sm-6">
<div class="feature-box9">
<div id="hexagon" class="left"><div id="hexagon2" class="left"><div class="icon"><i class="fa fa-line-chart"></i></div></div></div>
<div class="text-box-right">
<h3>Strategy</h3>
<p>Lorem ipsum dolor sit amet consectetuer<br>
elit et sit amet justo Suspendisse justo.</p>
</div></div></div>

<div class="clearfix"></div>

<div class="col-lg-6 col-md-6 col-sm-6">
<div class="feature-box9">
<div id="hexagon"><div id="hexagon2"><div class="icon"><i class="fa fa-bullhorn"></i></div></div></div>
<div class="text-box-left">
<h3>Marketing</h3>
<p>Lorem ipsum dolor sit amet consectetuer<br>
elit et sit amet justo Suspendisse justo.</p>
</div></div></div>

<div class="col-lg-6 col-md-6 col-sm-6">
<div class="feature-box9">
<div id="hexagon" class="left"><div id="hexagon2" class="left"><div class="icon"><i class="fa fa-cog"></i></div></div></div>
<div class="text-box-right">
<h3>SEO</h3>
<p>Lorem ipsum dolor sit amet consectetuer<br>
elit et sit amet justo Suspendisse justo.</p>
</div></div></div>

<div class="clearfix"></div>

<div class="col-lg-6 col-md-6 col-sm-6">
<div class="feature-box9">
<div id="hexagon"><div id="hexagon2"><div class="icon"><i class="fa fa-eyedropper"></i></div></div></div>
<div class="text-box-left">
<h3>Unlimited Colors</h3>
<p>Lorem ipsum dolor sit amet <br>
sit amet justo Suspendisse justo.</p>
</div></div></div>

<div class="col-lg-6 col-md-6 col-sm-6">
<div class="feature-box9">
<div id="hexagon" class="left"><div id="hexagon2" class="left"><div class="icon"><i class="fa fa-tablet"></i></div></div></div>
<div class="text-box-right">
<h3>Touch Support</h3>
<p>Lorem ipsum dolor sit amet <br>
elit et sit amet justo Suspendisse.</p>
</div></div></div>

</div>

 </div>
</section>

<!--end section-->
<div class="clearfix"></div>

<section class="section_category36">
 <div class="container">
 <div class="row"> 
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
 <div class="feature-box4 text-center">
 <div id="diamond4"><div class="diamond3 color4" ><div class="icon"><i class="fa fa-thumbs-up"></i></div></div></div> 
 <h3>Top-Notch Coding</h3>
 <div class="clearfix margin-top6"></div>
 <p>Lorem ipsum dolor sit amet, coper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse.</p>
 <div class="clearfix margin-top2"></div>
 <a href="#" class="btn btn-default">Read More</a>
 </div></div>
 
 <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
 <div class="feature-box4 text-center">
 <div id="diamond4"><div class="diamond3 color4" ><div class="icon"><i class="fa fa-magic"></i></div></div></div> 
 <h3>Web Development</h3>
 <div class="clearfix margin-top6"></div>
 <p>Lorem ipsum dolor sit amet, coper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse.</p>
 <div class="clearfix margin-top2"></div>
 <a href="#" class="btn btn-default">Read More</a>
 </div></div>
 
 <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
 <div class="feature-box4 text-center">
 <div id="diamond4"><div class="diamond3 color4" ><div class="icon"><i class="fa fa-tablet"></i></div></div></div> 
 <h3>Responsive Themes</h3>
 <div class="clearfix margin-top6"></div>
 <p>Lorem ipsum dolor sit amet, coper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse.</p>
 <div class="clearfix margin-top2"></div>
 <a href="#" class="btn btn-default">Read More</a>
 </div></div>
 
 <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
<div class="feature-box4 text-center">
 <div id="diamond4"><div class="diamond3 color4" ><div class="icon"><i class="fa fa-gamepad"></i></div></div></div> 
 <h3>Easy to Use</h3>
 <div class="clearfix margin-top6"></div>
 <p>Lorem ipsum dolor sit amet, coper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse.</p>
 <div class="clearfix margin-top2"></div>
 <a href="#" class="btn btn-default">Read More</a>
 </div></div>
  
 </div></div>
 </section>

<!--end section-->
<div class="clearfix margin-top8"></div>


<div id="why-choose">
<div class="container">
<div class="choose">
<div class="row">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/img1.jpg">
</div>

<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
<h2 class="uppercase">Why Choose Us?</h2>
<p class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse et justo. Praesent mattis commodo augue. Aliquam ornare hendrerit augue. Cras tellus. In pulvinar lectus a est. Lorem ipsum dolor sit amet .</p>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. lobortis pellentesque orci, in sodales nisi pretium sit ame Integer sodales suscipit tellus, ut tristique neque suscipit a. Mauris tristique lacus quis leo imperdiet sed pulvinar dui fermentum. Aenean sit amet diam non tortor sagittis varius. Aenean at lorem nulla, sit amet interdum nibh. Aliquam gravida odio nec dui ornare tempus elementum lectus rhoncus. Suspendisse lobortis pellentesque orci, in sodales nisi pretium sit amet. Aenean vulputate, odio non euismod eleifend, magna nisl elementum lorem lobortis pellentesque.</p>
<p class="text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse et justo. Praesent mattis commodo augue. Aliquam ornare hendrerit augue. Cras tellus. In pulvinar lectus a est. Lorem ipsum dolor sit amet .</p>
<div class="margin-top2"></div>
<a href="#" class="btn btn-danger">View More</a>
<div class="margin-top3"></div>
</div>


</div></div></div></div>
<div class="margin-top8"></div>
<div class="clearfix"></div>


<?php get_footer(); ?>