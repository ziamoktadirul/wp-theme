﻿<?php get_header(); ?>
  <div class="clearfix"></div>
  
  <div class="slider">
  <div class="titlebar five">
  <img src="<?php echo get_template_directory_uri(); ?>/images/banner11.jpg" class="img-responsive">
  <div class="container">
   <div class="breadcrumb">    
   <h1><?php the_title(); ?></h1>

</div></div></div></div>
   
 <!--end section-->
  <div class="clearfix"></div>
  
  <div class="margin-top2"></div> 
  
   <div class="clearfix"></div>

<section class="sec-padding">
    <div class="container">
      <div class="row">
	  <?php 
	  
	  $postfin = new WP_Query(array(
	  'post_type' => 'post',
	  ));
	  
	  while($postfin->have_posts()): $postfin->the_post(); ?>
        <div class="col-md-6 bmargin">
          <div class="blog-holder-12">
            <div class="image-holder">
              <div class="overlay bg-opacity-1"> <a href="#">
                <div class="icon"><i class="fa fa-search"></i></div>
                </a>
                <div class="post-date-box"><?php the_time('d'); ?></span> </div>
                <div class="post-date-box two"> <span><?php the_time('M,y'); ?></span> </div>
              </div>
              <?php the_post_thumbnail('postthum'); ?> </div>
          </div>
           <div class="clearfix"></div>
          <br/>
          
          <a href="#">
          <h3 class="less-mar1"><?php the_title(); ?></h3>
          </a>
          <div class="blog-post-info"> <span><i class="fa fa-user"></i> By <?php the_author(); ?></span> <span><i class="fa fa-comments-o"></i> 15 Comments</span> </div>
          <br/>
          <p><?php echo wp_trim_words(get_the_content(), 100, true); ?></p>
          <br/>
          <a class="btn btn-red-2 dark btn-round" href="<?php the_permalink(); ?>">Read more</a>
		  
		  <div class="clearfix"></div>
         <div class="col-divider-margin-6"></div>
        <div class=" divider-line solid light margin opacity-7"></div>
        <div class="col-divider-margin-6"></div>
		  
		  </div>
		    
        
		  <?php endwhile; ?>
      
   
<?php get_template_part('pagination'); ?>
        
        </div>
      </div>
    </div>
  </section>
 
<div class="clearfix"></div>

<?php get_footer(); ?>