<?php get_header(); ?>
  <!--end menu-->
 <div class="clearfix"></div>
  
  <?php get_template_part('slider'); ?>

 

<div class="clearfix"></div>

<section class="section_category16 center">
<div class="portfolio">
<div class="sec_title2">
<h1>Our Portfolio</h1> 
</div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis ut ligula quis ultricies. Integer fusce fringilla venenatis vulputate. </p>
</div>
<div class="clearfix"></div>

<div class="container">
<div class="row ">
<div class="portfolio-items">
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
<div class="portfolio-grid">
<a href="#" class="portfolio-content">
<img src="<?php echo get_template_directory_uri(); ?>/images/media/001.jpg" class="img-responsive" alt="">
<div class="portfolio-hover">
<div class="hover-content"><div>
<h5 class="portfolio-title">Torquatus dixit</h5>
<div class="portfolio-categories">
<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis ut ligula quis ultricies.</span>
</div></div></div></div></a></div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 animate-in">
<div class="portfolio-grid">
<a href="#" class="portfolio-content">
<img src="<?php echo get_template_directory_uri(); ?>/images/media/002.jpg" class="img-responsive" alt="">
<div class="portfolio-hover">
<div class="hover-content"><div>
<h5 class="portfolio-title">Torquatus dixit</h5>
<div class="portfolio-categories">
<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis ut ligula quis ultricies.</span>
</div></div></div></div></a></div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 animate-in">
<div class="portfolio-grid active">
<a href="#" class="portfolio-content">
<img src="<?php echo get_template_directory_uri(); ?>/images/media/003.jpg" class="img-responsive" alt="">
<div class="portfolio-hover">
<div class="hover-content"><div>
<h5 class="portfolio-title">Torquatus dixit</h5>
<div class="portfolio-categories">
<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis ut ligula quis ultricies.</span>
</div></div></div></div></a></div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 animate-in">
<div class="portfolio-grid">
<a href="#" class="portfolio-content">
<img src="<?php echo get_template_directory_uri(); ?>/images/media/004.jpg" class="img-responsive" alt="">
<div class="portfolio-hover">
<div class="hover-content"><div>
<h5 class="portfolio-title">Torquatus dixit</h5>
<div class="portfolio-categories">
<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis ut ligula quis ultricies.</span>
</div></div></div></div></a></div>
</div>


<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 animate-in" >
<div class="portfolio-grid">
<a href="#" class="portfolio-content">
<img src="<?php echo get_template_directory_uri(); ?>/images/media/005.jpg" class="img-responsive" alt="">
<div class="portfolio-hover">
<div class="hover-content"><div>
<h5 class="portfolio-title">Torquatus dixit</h5>
<div class="portfolio-categories">
<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis ut ligula quis ultricies.</span>
</div></div></div></div></a></div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 animate-in" >
<div class="portfolio-grid">
<a href="#" class="portfolio-content">
<img src="<?php echo get_template_directory_uri(); ?>/images/media/006.jpg" class="img-responsive" alt="">
<div class="portfolio-hover">
<div class="hover-content"><div>
<h5 class="portfolio-title">Torquatus dixit</h5>
<div class="portfolio-categories">
<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis ut ligula quis ultricies.</span>
</div></div></div></div></a></div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 animate-in" >
<div class="portfolio-grid">
<a href="#" class="portfolio-content">
<img src="<?php echo get_template_directory_uri(); ?>/images/media/007.jpg" class="img-responsive" alt="">
<div class="portfolio-hover">
<div class="hover-content"><div>
<h5 class="portfolio-title">Torquatus dixit</h5>
<div class="portfolio-categories">
<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis ut ligula quis ultricies.</span>
</div></div></div></div></a></div>
</div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 animate-in">
<div class="portfolio-grid">
<a href="#" class="portfolio-content">
<img src="<?php echo get_template_directory_uri(); ?>/images/media/008.jpg" class="img-responsive" alt="">
<div class="portfolio-hover">
<div class="hover-content"><div>
<h5 class="portfolio-title">Torquatus dixit</h5>
<div class="portfolio-categories">
<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis ut ligula quis ultricies.</span>
</div></div></div></div></a></div>
</div>


</div>
<a href="#" class="btn btn-border white uppercase margin-left-2"><span>View More Projects</span></a>

</div>
</div>
</section>

<!--end section-->
<div class="clearfix"></div>

<section class="section_category parallax-section27">
<div class="parallax-overlay">
<div class="container">

<div class="testimonials2 slide-controls-color-4 pink">
<h2 class="text-center uppercase text-white">What Our Client Say</h2>
<div class="margin-top2"></div>
<div class="clearfix"></div>
<div class="row">
<div class="col-md-1"></div>
<div class="col-md-10 text-center">
<div id="owl-demo2" class="owl-carousel ">
<div class="item">
<div class="testimonials4 ">
<div class="image-left">
<div class="client-img">
<div class="img-inner overflow-hidden"><img src="<?php echo get_template_directory_uri(); ?>/images/testi-img12.jpg" alt=""/></div>
</div>
</div>
<div class="description">
<p>Lorem ipsum dolor sit amet consectetuer adipiscing elit Suspendisse et justo Praesent mattis commodo augue Aliquam ornare hendrerit augue Cras tellus 
In pulvinar lectus a est Curabitur eget orci. Lorem ipsum dolor sit amet consectetuer adipiscing elit 
Suspendisse et justo Praesent mattis commodo.</p>
</div>
<div class="clearfix"></div>
<h5>Sam Rivers<br><span>CEO</span></h5>
</div>
</div>
<!--end item-->

<div class="item">
<div class="testimonials4">
<div class="image-left">
<div class="client-img">
<div class="img-inner overflow-hidden"><img src="<?php echo get_template_directory_uri(); ?>/images/team-img11.jpg" alt=""/></div>
</div>
</div>
<div class="description">
<p>Lorem ipsum dolor sit amet consectetuer adipiscing elit Suspendisse et justo Praesent mattis commodo augue Aliquam ornare hendrerit augue Cras tellus 
In pulvinar lectus a est Curabitur eget orci. Lorem ipsum dolor sit amet consectetuer adipiscing elit 
Suspendisse et justo Praesent mattis commodo.</p></div>                  
<div class="clearfix"></div>
<h5>Michel John <br><span>Manager</span></h5>
</div>
</div>
<!--end item-->

<div class="item">
<div class="testimonials4">
<div class="image-left">
<div class="client-img">
<div class="img-inner overflow-hidden"><img src="<?php echo get_template_directory_uri(); ?>/images/team-img1.jpg" alt=""/></div>
</div>
</div>
<div class="description">
<p>Lorem ipsum dolor sit amet consectetuer adipiscing elit Suspendisse et justo Praesent mattis commodo augue Aliquam ornare hendrerit augue Cras tellus 
In pulvinar lectus a est Curabitur eget orci. Lorem ipsum dolor sit amet consectetuer adipiscing elit 
Suspendisse et justo Praesent mattis commodo.</p></div>
<div class="clearfix"></div>                  
<h5>John William<br><span>Director</span></h5>
</div>
</div>
<!--end item--> 

<div class="item">
<div class="testimonials4">
<div class="image-left">
<div class="client-img">
<div class="img-inner overflow-hidden"><img src="<?php echo get_template_directory_uri(); ?>/images/team-img14.jpg" alt=""/></div>
</div>
</div>
<div class="description">
<p>Lorem ipsum dolor sit amet consectetuer adipiscing elit Suspendisse et justo Praesent mattis commodo augue Aliquam ornare hendrerit augue Cras tellus 
In pulvinar lectus a est Curabitur eget orci. Lorem ipsum dolor sit amet consectetuer adipiscing elit 
Suspendisse et justo Praesent mattis commodo.</p></div>
<div class="clearfix"></div>                
<h5>Brick Wall<br><span>Senior Manager</span></h5>
</div>
</div>
<!--end item-->

</div>
  
  </div>
  </div>

</div>

</div>
</div>
</section>

<div class="clearfix"></div>

<section class="margin-top3 padding-bottom-3">
<div class="container">
<div class="clientlogo-list white" >

	<div class="row">
		<div class="col-md-12">
    	    <div class="well"> 
                <div id="myCarousel" class="carousel slide">              
                 
                <!-- Carousel items -->
                <div class="carousel-inner">
                    
                <div class="item active">
                	<div class="row-fluid">
                	  <div class="span2"><a href="#" class="thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/019.png" alt="Image" style="max-width:100%;" /></a></div>
                	  <div class="span2"><a href="#" class="thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/020.png" alt="Image" style="max-width:100%;" /></a></div>
                	  <div class="span2"><a href="#" class="thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/021.png" alt="Image" style="max-width:100%;" /></a></div>
                	  <div class="span2"><a href="#" class="thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/022.png" alt="Image" style="max-width:100%;" /></a></div>
                      <div class="span2"><a href="#" class="thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/023.png" alt="Image" style="max-width:100%;" /></a></div>
                      <div class="span2"><a href="#" class="thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/024.png" alt="Image" style="max-width:100%;" /></a></div>
                	</div><!--/row-fluid-->
                </div><!--/item-->
                 
                <div class="item">
                	<div class="row-fluid">
                	  <div class="span2"><a href="#" class="thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/019.png" alt="Image" style="max-width:100%;" /></a></div>
                	  <div class="span2"><a href="#" class="thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/020.png" alt="Image" style="max-width:100%;" /></a></div>
                	  <div class="span2"><a href="#" class="thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/021.png" alt="Image" style="max-width:100%;" /></a></div>
                	  <div class="span2"><a href="#" class="thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/022.png" alt="Image" style="max-width:100%;" /></a></div>
                      <div class="span2"><a href="#" class="thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/023.png" alt="Image" style="max-width:100%;" /></a></div>
                      <div class="span2"><a href="#" class="thumbnail"><img src="<?php echo get_template_directory_uri(); ?>/images/clients/024.png" alt="Image" style="max-width:100%;" /></a></div>
                	</div><!--/row-fluid-->
                </div><!--/item-->
                 
             
                 
                </div><!--/carousel-inner-->
                 
                <a class="left carousel-control pink" href="#myCarousel" data-slide="prev"><i class="fa fa-caret-left"></i></a>
                <a class="right carousel-control pink" href="#myCarousel" data-slide="next"><i class="fa fa-caret-right"></i></a>
                </div><!--/myCarousel-->
                 
            </div><!--/well-->   
		</div>

</div>

</div></div>
</section>
<!--end section-->
<?php get_footer(); ?>