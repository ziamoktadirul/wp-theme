



<div class="slider">
  <!-- masterslider -->
  <div class="master-slider ms-skin-default" id="masterslider"> 
    
    <!-- slide 1 -->
    <div class="ms-slide slide-1 text-center" data-delay="9"> 
    <img src="<?php echo get_template_directory_uri(); ?>/js/masterslider/blank.gif" data-src="images/slider/slide5.jpg" alt=""/>
         
      <h3 class="ms-layer text79"
			style="left: 0px;top: 160px;"
			data-type="text"
			data-delay="500"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="left(250)"> Creative Solutions</h3>
            
      <h3 class="ms-layer text80"
			style="left:0px;top: 230px;"
			data-type="text"
			data-delay="1000"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="scale(1.5,1.6)">We Help You Grow Up Your Business</h3>
            
                 
      <h3 class="ms-layer text78"
        	style="left:0px; top: 300px;"
            data-type="text"
            data-effect="top(45)"
            data-duration="2000"
            data-delay="1500"
            data-ease="easeOutExpo"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse et justo. Praesent mattis<br>commodo augue. Aliquam ornare hendrerit augue. Cras tellus. </h3>
            
             <a class="ms-layer sbut4"
			style="left: 708px; top: 400px;"
			data-type="text"
			data-delay="2000"
			data-ease="easeOutExpo"
			data-duration="1200"
			data-effect="left(250)"> <span>View Details</span> </a> 
            
           
            
     
    </div>
    <!-- end slide 1 --> 
    
    <!-- slide 2 -->
    <div class="ms-slide slide-3" data-delay="9">
         
        <!-- slide background -->
        <img src="<?php echo get_template_directory_uri(); ?>/images/slider/slide6.jpg" alt=""/>
        
		<h3 class="ms-layer text1"
			style="right:230px;top:160px;"
			data-type="text"
			data-ease="easeOutExpo"
			data-delay="500"
		 	data-duration="1400"
		 	data-effect="skewleft(30,80)"> Video Layer With</h3>
            
      <h3 class="ms-layer text2"
			style="right:230px;top:215px;"
			data-type="text"
			data-ease="easeOutExpo"
			data-delay="1000"
		 	data-duration="1400"
		 	data-effect="skewright(30,80)"> Video Custom Cover </h3>
            
      <h3 class="ms-layer text4"
        	style="right: 230px; top: 283px;"
            data-type="text"
            data-effect="top(45)"
            data-duration="2000"
            data-delay="1500"
            data-ease="easeOutExpo"> Lorem ipsum dolor sit amet consectetuer adipiscing <br /> elit Suspendisse et justo 
        Praesent. </h3>
        
      <a class="ms-layer sbut5 blue"
			style="right: 220px; top: 365px;"
			data-type="text"
			data-delay="2000"
			data-ease="easeOutExpo"
			data-duration="1200"
			data-effect="scale(1.5,1.6)"> View More </a> 
            
            <a class="ms-layer sbut5"
			style="right: 390px; top: 365px;"
			data-type="text"
			data-delay="2500"
			data-ease="easeOutExpo"
			data-duration="1200"
			data-effect="scale(1.5,1.6)"> Buy Now ! </a> 
		
		<img src="masterslider/blank.html" data-src="images/slider/video-shadow.png" alt="video shadow"
		 	  style="left:160px; top:420px;"
		 	  class="ms-layer"
		 	  data-type="image"
			  data-duration="3000"
			  data-ease="easeOutExpo"
		/>
		
		<div class="ms-layer video-box" style="left:210px; top:90px; width:662px; height:372px"
			  data-type="video"
			  data-effect="top(100)"
			  data-duration="3000"
			  data-ease="easeOutExpo"
		>
			<img src="masterslider/blank.html" data-src="images/slider/video-cover.jpg" alt="video shadow"/>
            <iframe src="https://www.youtube.com/embed/dqHyXb5Okho?modestbranding=1&amp;autohide=1&amp;showinfo=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			<!--<iframe src="http://player.vimeo.com/video/50672540" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen> </iframe>-->
		</div>

    </div>
    <!-- end slide 2 -->
	
    <!-- slide 3 -->
    <div class="ms-slide slide-3" data-delay="9">
      
      <img src="<?php echo get_template_directory_uri(); ?>/js/masterslider/blank.gif" data-src="images/slider/slide7.jpg" alt=""/>
      
      <h3 class="ms-layer text44"
			style="right: 230px;top: 180px;"
			data-type="text"
			data-delay="500"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="scale(1.5,1.6)"> One Page Layouts & </h3>
            
      <h3 class="ms-layer text45"
			style="right: 230px;top: 245px;"
			data-type="text"
			data-delay="1000"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="scale(5.5,1.6)"> Many More Features </h3>
            
      <h3 class="ms-layer text7"
        	style="right: 230px; top: 330px;"
            data-type="text"
            data-effect="top(45)"
            data-duration="2000"
            data-delay="1500"
            data-ease="easeOutExpo"> Lorem ipsum dolor sit amet consectetuer adipiscing elit Suspendisse et justo <br />
        Praesent mattis commodo augue Aliquam ornare. </h3>
        
      <a class="ms-layer sbut5"
			style="right: 220px; top: 420px;"
			data-type="text"
			data-delay="2000"
			data-ease="easeOutExpo"
			data-duration="1200"
			data-effect="scale(1.5,1.6)"> Read More </a> 
            
            <a class="ms-layer sbut7"
			style="right: 390px; top: 420px;"
			data-type="text"
			data-delay="2500"
			data-ease="easeOutExpo"
			data-duration="1200"
			data-effect="scale(1.5,1.6)"> Buy Now ! </a> 
            
            </div>
    <!-- end slide 3 -->
    
  </div>
  <!-- end of masterslider -->
   </div>