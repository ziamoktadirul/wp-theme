<?php

/**
 * Finger functions and definitions
 * Finger is a Corporate Theme to use in our Multipurpose Porjects
 * such as company website
 **/


 
 require_once('finger-nav-walker.php');
 require_once('inc/ReduxCore/framework.php');
 require_once('inc/sample/custom.php');
 require_once('shortcode/shortcode.php');
 require_once('metabox/init.php');
 require_once('metabox/custom.php');
 require_once('widget/finger-recent-widget.php');
 
 
if(!function_exists('shipper_setup_functions')) :

function shipper_setup_functions(){



register_post_type('Aboutmilestone', array(
	'labels' => array(
		'name' => 'Aboutmilestone',
		'add new' => 'add new Aboutmilestone',
		'add new item' => 'add new item',
	
	
	),
	'public'=> true,
	'supports' => array('title', 'editor', 'thumbnail'),

));
register_post_type('aboutclient', array(
	'labels' => array(
		'name' => 'aboutclient',
		'add new' => 'add new aboutclient',
		'add new item' => 'add new item',
	
	
	),
	'public'=> true,
	'supports' => array('title', 'editor', 'thumbnail'),

));

register_post_type('fcarosul', array(
	'labels' => array(
		'name' => 'fcarosul',
		'add new' => 'add new fcarosul',
		'add new item' => 'add new item',
	
	
	),
	'public'=> true,
	'supports' => array('title', 'editor', 'thumbnail'),

));


register_post_type('client', array(
	'labels' => array(
		'name' => 'client',
		'add new' => 'add new client',
		'add new item' => 'add new item',
	
	
	),
	'public'=> true,
	'supports' => array('title', 'editor', 'thumbnail'),

));


register_post_type('aboutpromo', array(
	'labels' => array(
		'name' => 'aboutpromo',
		'add new' => 'add new aboutpromo',
		'add new item' => 'add new item',
	
	
	),
	'public'=> true,
	'supports' => array('title', 'editor', 'thumbnail'),

));

register_post_type('promo', array(
	'labels' => array(
		'name' => 'promo',
		'add new' => 'add new promo',
		'add new item' => 'add new item',
	
	
	),
	'public'=> true,
	'supports' => array('title', 'editor', 'thumbnail'),

));

 register_taxonomy('promotion-category', 'promo', array(
 
 'labels' => array(
	'name' => 'Categories',
	'add_new_item' => 'Add New Category'
 ),
 'public'=> true,
 'hierarchical' => true
 
 ));
 
register_post_type('portfolio', array(
	'labels' => array(
		'name' => 'portfolio',
		'add new' => 'add new portfolio',
		'add new item' => 'add new item',
	
	
	),
	'public'=> true,
	'supports' => array('title', 'editor', 'thumbnail'),

));

 register_taxonomy('portfolio-category', 'portfolio', array(
 
 'labels' => array(
	'name' => 'Categories',
	'add_new_item' => 'Add New Category'
 ),
 'public'=> true,
 'hierarchical' => true
 
 ));

 
 

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on twentyfifteen, use a find and replace
	 */
	load_theme_textdomain( 'shipper', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );


	/* Title Tag */

	add_theme_support('title-tag');

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 */

	add_theme_support('post-thumbnails');
	add_image_size('thum', 270,300, true);
	add_image_size('postthum', 550,350, true);
	add_image_size('abclient', 238,191, true);
	/* Main Menu */

	register_nav_menu('main-menu', __('Main Menu', 'fingertech'));/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );





}

endif;
add_action('after_setup_theme', 'shipper_setup_functions');


/* Enqueue Styles */

add_action('wp_enqueue_scripts', 'fingertech_styles');

function fingertech_styles(){
	
	wp_enqueue_style('bootstrapcss', get_template_directory_uri().'/js/bootstrap/css/bootstrap.min.css');
	wp_enqueue_style('mainmenucss', get_template_directory_uri().'/js/mainmenu/menu-2.css');
	wp_enqueue_style('theme-defaultcss', get_template_directory_uri().'/css/theme-default.css');
	wp_enqueue_style('theme-style', get_template_directory_uri().'/css/theme-style.css');
	wp_enqueue_style('shortcodescss', get_template_directory_uri().'/css/shortcodes.css');
	wp_enqueue_style('font-awesome', get_template_directory_uri().'/css/font-awesome/css/font-awesome.min.css');
	wp_enqueue_style('masterslidercss', get_template_directory_uri().'/js/masterslider/style/masterslider.css');
	wp_enqueue_style('animationsmincss', get_template_directory_uri().'/js/animations/css/animations.min.css');
	wp_enqueue_style('owlcarouselcss', get_template_directory_uri().'/js/owl-carousel/owl.carousel.css');
	wp_enqueue_style('responsive-tabs-css', get_template_directory_uri().'/js/tabs/assets/css/responsive-tabs.css');
	wp_enqueue_style('simplelightboxmincss', get_template_directory_uri().'/js/simplelightbox/simplelightbox.min.css');
	wp_enqueue_style('responsive-stylecss', get_template_directory_uri().'/css/responsive-style.css');
	wp_enqueue_style('style-swicher-css', get_template_directory_uri().'/js/style-swicher/style-swicher.css');
	wp_enqueue_style('bootstrap-combined-css', get_template_directory_uri().'/js/bootstrap/css/bootstrap-combined.css');
	wp_enqueue_style('smart-forms-css', get_template_directory_uri().'/js/smart-forms/smart-forms.css');
	
	wp_enqueue_style('stylesheet', get_stylesheet_uri());
	
}
function fingertech_scripts(){
	
	wp_enqueue_script('bootstrapjs', get_template_directory_uri().'/js/bootstrap/js/bootstrap.min.js', array( 'jquery' ));
	wp_enqueue_script('customeUI-js', get_template_directory_uri().'/js/mainmenu/customeUI.js', array( 'jquery' ));
	wp_enqueue_script('accordion-js', get_template_directory_uri().'/js/tabs/smk-accordion.js', array( 'jquery' ));
	wp_enqueue_script('custom-js', get_template_directory_uri().'/js/tabs/custom.js', array( 'jquery' ));
	wp_enqueue_script('sticky-js', get_template_directory_uri().'/js/mainmenu/jquery.sticky.js', array( 'jquery' ));
	wp_enqueue_script('masterslidermin-js', get_template_directory_uri().'/js/masterslider/masterslider.min.js', array( 'jquery' ));
	wp_enqueue_script('YTPlayer-js', get_template_directory_uri().'/js/ytplayer/jquery.mb.YTPlayer.js', array( 'jquery' ));
	wp_enqueue_script('elementvideo-customjs', get_template_directory_uri().'/js/ytplayer/elementvideo-custom.js', array( 'jquery' ));
	wp_enqueue_script('play-pause-btn-js', get_template_directory_uri().'/js/ytplayer/play-pause-btn.js', array( 'jquery' ));
	wp_enqueue_script('cubeportfolio-min-js', get_template_directory_uri().'/js/cubeportfolio/jquery.cubeportfolio.min.js', array( 'jquery' ));
	wp_enqueue_script('main-js', get_template_directory_uri().'/js/cubeportfolio/main.js', array( 'jquery' ));
	wp_enqueue_script('animations-min-js', get_template_directory_uri().'/js/animations/js/animations.min.js', array( 'jquery' ));
	wp_enqueue_script('appear-min-js', get_template_directory_uri().'/js/animations/js/appear.min.js', array( 'jquery' ));
	wp_enqueue_script('totop.js', get_template_directory_uri().'/js/scrolltotop/totop.js', array( 'jquery' ));
	wp_enqueue_script('owl-carousel-js', get_template_directory_uri().'/js/owl-carousel/owl.carousel.js', array( 'jquery' ));
	wp_enqueue_script('customjs', get_template_directory_uri().'/js/owl-carousel/custom.js', array( 'jquery' ));
	wp_enqueue_script('concat-min-js', get_template_directory_uri().'/js/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js', array( 'jquery' ));
	wp_enqueue_script('swicher-js', get_template_directory_uri().'/js/style-swicher/style-swicher.js', array( 'jquery' ));
	wp_enqueue_script('swicher-custom-js', get_template_directory_uri().'/js/style-swicher/custom.js', array( 'jquery' ));
	wp_enqueue_script('functions-js', get_template_directory_uri().'/js/scripts/functions.js', array( 'jquery' ));
	wp_enqueue_script('additional-methodsminjs', get_template_directory_uri().'/js/smart-forms/additional-methods.min.js', array( 'jquery' ));
	wp_enqueue_script('jquery-form-min-js', get_template_directory_uri().'/js/smart-forms/jquery.form.min.js', array( 'jquery' ));
	wp_enqueue_script('jquery-validate-min-js', get_template_directory_uri().'/js/smart-forms/jquery.validate.min.js', array( 'jquery' ));
	wp_enqueue_script('smart-form-js', get_template_directory_uri().'/js/smart-forms/smart-form.js', array( 'jquery' ));
	wp_enqueue_script('examples-js', get_template_directory_uri().'/js/gmaps/examples.js', array( 'jquery' ));
	wp_enqueue_script('jquery-gmap-js', get_template_directory_uri().'/js/gmaps/jquery.gmap.js', array( 'jquery' ));
	wp_enqueue_script('mapapijs', get_template_directory_uri().'/js/gmaps/maps.js', array( 'jquery' ));

}

add_action('wp_enqueue_scripts', 'fingertech_scripts');


function default_menu(){
	
		echo'<ul class="nav navbar-nav">';
        echo'<li class="dropdown"> <a href="'.home_url().'" class="dropdown-toggle active">Home</a>';
		echo '</ul>';
	
	
}

add_action('widgets_init', 'footer_sidebar_widgets');
function Footer_sidebar_widgets(){
	register_sidebar(array(
		'name' => __('Right Sidebar', 'fingertech'),
		'id' => 'finger-footer-sidebar',
		'description' => __('FIngertech Footer Sidebar', 'Fingertech'),
		'before_widget' => '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 colmargin" >',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="footer-title2">',
		'after_title' => '</h3>',
	));
	
register_sidebar(array(
		'name' => __('Footer latest post sidebar', 'fingertech'),
		'id' => 'finger-latest-post-sidebar',
		'description' => __('FIngertech Footer Sidebar', 'Fingertech'),
		'before_widget' => '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 colmargin" >',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="footer-title2">',
		'after_title' => '</h3>',
	));






}




// visual composer support

if(function_exists('vc_map')){
	vc_map(array(
		'name' => 'FingerTech Promo',
		'base' => 'promo',
	
	));
	
	vc_map(array(
		'name' => 'FingerTech Portfolio',
		'base' => 'portfolio',
		'params' => array(
		
			array(
			'type' => 'textfield',
			'param_name' => 'title',
			'value' => 'Our Portfolio',
			'heading' => 'Title'
			
			),
			array(
			'type' => 'textfield',
			'param_name' => 'cont',
			'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mattis ut ligula quis ultricies',
			'heading' => 'Content'
			
			)
			
		
		)
	
	));
	
	vc_map(array(
		'name' => 'Finger Client',
		'base' => 'client',
		'params' => array(
			array(
				'type' => 'textfield',
				'param_name' => 'title',
				'value' => 'What Our Client Say',
				'heading' => 'Title'
			)
		)
	));
	
	vc_map(array(
		'name' => 'FingerTech Fcarosul',
		'base' => 'fcarosul'
	
	));
	
}


