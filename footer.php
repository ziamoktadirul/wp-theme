
<section class="section-dark sec-padding">
<div class="container">
<div class="row">

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 colmargin " >
 <div class="footer-logo2"><img src="<?php echo get_template_directory_uri(); ?>/images/fff.png" alt=""></div>
          <ul class="address-info-2 no-border">
            <li><i class="fa fa-map-marker"></i> No.28 - 63739 street lorem ipsum City, Country 98133-9099</li>
            <li><i class="fa fa-phone-square"></i> + 1 (234) 567 8901</li>
            <li><i class="fa fa-mobile"></i> + 1 (234) 567 8901</li>
            <li><i class="fa fa-fax"></i> + 1 (234) 567 8901</li>
            <li class="last"><i class="fa fa-envelope"></i> support@yourdomain.com </li>
          </ul>
         <div class="clearfix"></div> 
          <ul class="social-icons-3">
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
            </ul>
        </div>
     
	<?php dynamic_sidebar('finger-footer-sidebar'); ?> 


<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 colmargin" >
<h3 class="footer-title2">Newsletter</h3>
<!-- <div class="footer-title2-bottomstrip"></div> -->
<div class="clearfix"></div>
<p class="text-center">Subscribe to Our Newsletter to get 
Important News,  Keep up on our always 
evolving product features & technology.</p>
<div class="newsletter">
<input name="" type="text" placeholder="Enter Your Email Address" class="email_input2">
<input name="" type="button" value="Subscribe" class="input_submit2">
</div>
<div class="clearfix margin-top6"></div>
</div>

<?php dynamic_sidebar('finger-latest-post-sidebar'); ?>




</div>
</div>
</section>


<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
</div>


<div id="style-selector" class="text-center"> <a href="javascript:void(0);" class="btn-close"><i class="fa fa-wrench"></i></a>
  <h5 class="title-big">Choose Theme Options</h5>
  <div class="style-selector-wrapper">
    <h5 class="title">Choose Layout</h5>
    <div class="clearfix"></div>
   <a class="btn-gray active" href="index.html">Wide</a> 
   <a class="btn-gray" href="">Boxed</a>
    
    <div class="clearfix"></div>
    <br/>
    <h5 class="title align-left">Predefined Color Skins</h5>
    <ul class="pre-colors-list">
      <li><a title="default" href="css/colors/default.css"><span class="pre-color-skin-1"></span></a></li>
      <li><a title="light blue" href="css/colors/lightblue.css"><span class="pre-color-skin-2"></span></a></li>
      <li><a title="orange" href="css/colors/orange.css"><span class="pre-color-skin-4"></span></a></li>
      <li><a title="green" href="css/colors/green.css"><span class="pre-color-skin-3"></span></a></li>
      <li><a title="pink" href="css/colors/pink.css"><span class="pre-color-skin-5"></span></a></li>
      <li class="last"><a title="red" href="css/colors/red.css"><span class="pre-color-skin-6"></span></a></li>
      <li><a title="purple" href="css/colors/purple.css"><span class="pre-color-skin-7"></span></a></li>
      <li><a title="bridge" href="css/colors/bridge.css"><span class="pre-color-skin-8"></span></a></li>
      <li><a title="yellow" href="css/colors/yellow.css"><span class="pre-color-skin-9"></span></a></li>
      <li><a title="violet" href="css/colors/violet.css"><span class="pre-color-skin-10"></span></a></li>
      <li><a title="blue" href="css/colors/blue.css"><span class="pre-color-skin-11"></span></a></li>
      <li class="last"><a title="moss green" href="css/colors/mossgreen.css"><span class="pre-color-skin-12"></span></a></li>
    </ul>
    <h5 class="title align-left">BG Patterns for Boxed</h5>
    <ul class="bg-pattrens-list">
      <li><a href="css/bg-patterns/pattern-1.css"><span class="bg-pattren-1"></span></a></li>
      <li><a href="css/bg-patterns/pattern-2.css"><span class="bg-pattren-2"></span></a></li>
      <li><a href="css/bg-patterns/pattern-3.css"><span class="bg-pattren-3"></span></a></li>
      <li><a href="css/bg-patterns/pattern-4.css"><span class="bg-pattren-4"></span></a></li>
      <li><a href="css/bg-patterns/pattern-5.css"><span class="bg-pattren-5"></span></a></li>
      <li class="last"><a href="css/bg-patterns/pattern-6.css"><span class="bg-pattren-6"></span></a></li>
      <li><a href="css/bg-patterns/pattern-7.css"><span class="bg-pattren-7"></span></a></li>
      <li><a href="css/bg-patterns/pattern-8.css"><span class="bg-pattren-8"></span></a></li>
      <li><a href="css/bg-patterns/pattern-9.css"><span class="bg-pattren-9"></span></a></li>
      <li><a href="css/bg-patterns/pattern-10.css"><span class="bg-pattren-10"></span></a></li>
      <li><a href="css/bg-patterns/pattern-11.css"><span class="bg-pattren-11"></span></a></li>
      <li class="last"><a href="css/bg-patterns/pattern-12.css"><span class="bg-pattren-12"></span></a></li>
    </ul>
  </div>
</div>


<?php wp_footer(); ?>
</body>
</html>