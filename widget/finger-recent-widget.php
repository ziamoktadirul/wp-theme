<?php
add_action('widgets_init', 'recent_post');
 function recent_post(){
	 register_widget('finger_recent_post_widget');
	 
 }
class finger_recent_post_widget extends wp_widget{
	
	public function __construct(){
		parent::__construct('recent-post-widget', 'Fingertech Latest Post', array(
			'description' => 'Fingertech latest post',
		));
		
	}
	
	public function widget($a, $dta){
		
		ob_start();
		?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 colmargin" >
<h3 class="footer-title2"><?php echo $dta['title']; ?></h3>
<!-- <div class="footer-title2-bottomstrip"></div> -->
<div class="clearfix"></div>
<?php 
$pos = new WP_Query(array(
'post_type' => 'post',
'posts_per_page' => $dta['post-count']

));

while($pos->have_posts()): $pos->the_post(); ?>
          <div class="image-left">
            <div class="fo-postimg-inner2 overflow-hidden"><?php the_post_thumbnail(); ?></div>
          </div>
          <div class="text-box-right">
            <h5 class="text-white less-mar3"><a href="#"><?php the_title(); ?></a></h5>
            <span>Lorem ipsum dolor sit</span>
            <div class="footer-post-info"> <span>By <?php the_author(); ?></span><span><?php the_time('M,d'); ?></span> </div>
          </div>
          <div class="divider-line dashed dark-2 margin "></div>
       <?php endwhile; ?>
         
        </div>
		<?php echo ob_get_clean();
	}
	
public function form( $dta ){ 

	?>
		
		<p>
			<label for="">Title:
				<input type="text" id="<?php echo $this->get_field_id('title'); ?>" class="widefat" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $dta['title']; ?>">
			</label>
		<label for="">Post Count:
				<select name="<?php echo $this->get_field_name('post-count'); ?>" id="">
					
					<option value="1" <?php if($dta['post-count'] == 1) {echo "selected";} ?>>1</option>
					<option value="2" <?php if($dta['post-count'] == 2) {echo "selected";} ?>>2</option>
					<option value="3" <?php if($dta['post-count'] == 3) {echo "selected";} ?>>3</option>
					<option value="4" <?php if($dta['post-count'] == 4) {echo "selected";} ?>>4</option>
					<option value="5" <?php if($dta['post-count'] == 5) {echo "selected";} ?>>5</option>
				</select>
			</label>


			
		</p>

	<?php }

}