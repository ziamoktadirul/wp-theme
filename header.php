
<?php
global $fingertech;

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" type="image/x-icon" />
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png">

<!-- Web Fonts  -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800,600|Lato:400,300,700,900' rel='stylesheet' type='text/css'>


<!-- Remove the below comments to use your color option -->
<!--<link rel="stylesheet" href="css/colors/lightblue.css" />-->
<!--<link rel="stylesheet" href="css/colors/orange.css" />-->
<!--<link rel="stylesheet" href="css/colors/green.css" />-->
<!--<link rel="stylesheet" href="css/colors/pink.css" />-->
<!--<link rel="stylesheet" href="css/colors/red.css" />-->
<!--<link rel="stylesheet" href="css/colors/purple.css" />-->
<!--<link rel="stylesheet" href="css/colors/bridge.css" />-->
<!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
<!--<link rel="stylesheet" href="css/colors/violet.css" />-->
<!--<link rel="stylesheet" href="css/colors/blue.css" />-->
<!--<link rel="stylesheet" href="css/colors/mossgreen.css" />-->
<?php wp_head(); ?>
</head>

<body  <?php body_class(); ?>>
<div class="site_wrapper font-style2">

<div class="topbar light">
    <div class="container">
      <div class="topbar-left-items">
        <ul class="toplist pull-left">
          <li class="lineright"><i class="fa fa-phone"></i> <?php echo $fingertech['mobail_no']; ?>  </li>
          <li class="lineright"><a href="mailtnfo@yourdomain.com"><i class="fa fa-envelope"></i> <?php echo $fingertech['mobail_info']; ?> </a></li>
          <li><a href="login.html"><i class="fa fa-user"></i> Login / Register</a></li>
         
        </ul>
      </div>
      <!--end left-->
      
      <div class="topbar-right-items pull-right">
        <ul class="toplist social-icons-1">          
        <?php if(!empty($fingertech['face_no'])) : ?> <li><a href="<?php $fingertech['face_no']; ?>"><i class="fa fa-facebook"></i></a></li><?php endif; ?>
          <?php if(!empty($fingertech['twitter_info'])) : ?><li><a href="<?php $fingertech['twitter_info']; ?>"><i class="fa fa-twitter"></i></a></li><?php endif; ?>
          <?php if(!empty($fingertech['google_plus'])) : ?><li><a href="<?php $fingertech['google_plus']; ?>"><i class="fa fa-google-plus"></i></a></li><?php endif; ?>
          <?php if(!empty($fingertech['link_logo'])) : ?><li><a href="<?php $fingertech['link_logo']; ?>"><i class="fa fa-linkedin"></i></a></li><?php endif; ?>
         <?php if(!empty($fingertech['prin_plus'])) : ?> <li class="last"><a href="<?php $fingertech['prin_plus']; ?>"><i class="fa fa-pinterest"></i></a></li><?php endif; ?>
        </ul>
      </div>
    </div>
  </div>
  
  <div class="clearfix"></div>

<div id="header2">
    <div class="container">
      <div class="navbar navbar-default yamm">
      
        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle two three"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
          <a href="<?php home_url(); ?>" class="navbar-brand"><img src="<?php echo $fingertech['head_logo']['url']; ?>" alt=""/></a> </div>
          	 
       <div id="navbar-collapse-grid" class="navbar-collapse collapse pull-right">
		 <?php wp_nav_menu(array(
            'theme_location' => 'main-menu',
            'menu_class' => 'nav navbar-nav',
			'fallback_cb' => 'default_menu',
			'walker' => new Finger_Nav_walker(),
			
         
          ));

		  ?>
	
           <!--<ul class="nav navbar-nav">
            <li class="dropdown"> <a href="index.html" class="dropdown-toggle active">Home</a>
              
            </li>
            
            <li class="dropdown yamm-fw"> <a href="about2.html" class="dropdown-toggle">Pages</a>
              
            </li>
            <li class="dropdown yamm-fw"> <a href="message-boxes.html" class="dropdown-toggle">Features</a>
              
            </li>
            <li class="dropdown"> <a href="portfolio-three.html" class="dropdown-toggle">Portfolio</a>
   
            </li>
            <li class="dropdown yamm-fw"> <a href="#" class="dropdown-toggle">Shortcodes</a>
              
            </li>
            <li class="dropdown"> <a href="blog.html" class="dropdown-toggle">Blog</a>
              
            </li>
            <li class="dropdown"> <a href="contact.html" class="dropdown-toggle align-1">Contact</a>
              <ul class="dropdown-menu align-1 two" role="menu">
                <li> <a href="contact.html">Contact Variation 1</a> </li>
                <li> <a href="contact2.html">Contact Variation 2</a> </li>
                <li> <a href="contact3.html">Contact Variation 3</a> </li>
              </ul>
            </li>
            
          </ul>-->
        </div> 
      </div>
    </div>
  </div>