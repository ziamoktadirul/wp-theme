﻿<?php 
/*
Template Name:about

*/


get_header(); ?>
  <!--end menu-->
 <!-- <div class="clearfix"></div>-->
 
 <div class="titlebar two">
  <div class="container">
   <div class="breadcrumb">
    <div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6"><h1>About Style 2</h1></div>
<div class="col-lg-6 col-md-6 col-sm-6">
<div class="pagenation"><a href="#">Home</a> <i class="fa fa-angle-right"></i> <a href="#">Pages</a> <i class="fa fa-angle-right"></i> About Us</div></div>
      </div></div></div>
      
    
   </div>
  
  
  <div class="clearfix"></div>
 <!--end section-->
 
 
 <section class="section_category12 less-p">
 <div class="container">
 <div class="row"> 
 <?php 
 
 $aboutp = new WP_Query(array(
 'post_type' => 'aboutpromo',
 'posts_per_page' => 3,
 
 ));
 
 while($aboutp->have_posts()): $aboutp->the_post(); ?>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
 <div class="feature-box3 blue text-center">
 <div class="iconbox-large round dark-outline"><i class="fa fa-<?php echo get_post_meta(get_the_id(), '__aboutpi__', true); ?>"></i></div>
 <h3 class="margin-top2"><?php the_title(); ?></h3>
 <p><?php echo wp_trim_words(get_the_content(), 100, ''); ?></p>
 <a href="<?php the_permalink(); ?>" class="btn btn-default">View Details</a>
 </div>
 </div>
 
 <?php endwhile; ?>

 

  
 </div></div>
 </section>
<div class="clearfix"></div>

<section class="section_category13">
 <div class="container">
 <div class="row">
 <div class="col-lg-12  center">
 <div  class="sec_title2 " ><h1><?php echo $fingertech['title_lo']; ?></h1></div>
 <p class="" data-anim-type="fade-in-left" data-anim-delay="100"><?php echo $fingertech['content_info']; ?></p>
 </div>
</div>
<div class="margin-top4"></div>
</div>
</section>

<div class="container">

<div class="team_box">
<div class="row">

<?php 

$boutclient = new WP_Query(array(

'post_type' => 'aboutclient',
'posts_per_page' => 4,

));

while($boutclient->have_posts()): $boutclient->the_post(); ?>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 " >
<div class="team">
<div class="team-img">
<?php the_post_thumbnail('abclient'); ?>
</div>
<h3><?php the_title(); ?></h3>
<p><?php the_content(); ?></p>
<div class="social">
<?php if(!empty($fingertech['faceboo_lo'])): ?><a href="<?php echo $fingertech['faceboo_lo']; ?>"><i class="fa fa-facebook"></i></a><?php endif; ?>
<?php if(!empty($fingertech['twitter_num'])): ?><a href="<?php echo $fingertech['twitter_num']; ?>"><i class="fa fa-twitter"></i></a><?php endif; ?>
<?php if(!empty($fingertech['goole_info'])): ?><a href="<?php echo $fingertech['goole_info']; ?>"><i class="fa fa-google-plus"></i></a><?php endif; ?>
</div>
</div>
</div>

<?php endwhile; ?>





</div>
</div>
</div>

<!--end section-->
<div class="clearfix margin-top4 mb-5"></div>





<!--end section-->
<div class="clearfix"></div>

<div class="container">
<div  class="sec_title2"><h1><?php echo $fingertech['title_mile']; ?></h1></div>
<p class="font-weight-3 text-center"><?php echo $fingertech['content_mile']; ?></p>
<div class="bottom-margin4"></div>
<div class="row">
<?php 

$mile = new WP_Query(array(

'post_type' => 'Aboutmilestone',

'posts_per_page' => 3,
));


while($mile->have_posts()): $mile->the_post(); ?>

<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" data-anim-type="fade-in-up" data-anim-delay="100">
<div class="milestones"><?php the_post_thumbnail(); ?>
<div class="content-bar">
<div class="icon"><i class="fa fa-photo"></i></div>
<h4><?php the_title(); ?></h4>
<p><?php the_content(); ?></p>
</div>
</div>
</div>

<?php endwhile; ?>




</div>
</div>

<!--end section-->
<div class="clearfix"></div>

<section class="section_category9 top_less">
<div class="container">
<div class="row">
<div class="col-md-12 " data-anim-type="fade-in-right" data-anim-delay="100">
<div  class="sec_title2"><h1>Our Pricing</h1></div>
</div></div>
<div class="clearfix"></div>
<div class="row">

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 " >
<div class="pricetable">
<div class="title uppercase">Basic</div>
<div class="clearfix"></div>
<div class="price-box">
<div class="price">$9.99<span>/Month</span></div>
</div>
<ul class="plan_features">
<li>1 GB Bandwidth</li>
<li>256 MB Memory</li>
<li>Unlimited</li>
<li>Full Support</li>
<li>Free Domain</li>
</ul>
<a class="btn" href="#">Sign Up</a>
</div></div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 " >
<div class="pricetable">
<div class="title uppercase">Standard</div>
<div class="clearfix"></div>
<div class="price-box">
<div class="price">$29.99<span>/Month</span></div>
</div>
<ul class="plan_features">
<li>1 GB Bandwidth</li>
<li>256 MB Memory</li>
<li>Unlimited</li>
<li>Full Support</li>
<li>Free Domain</li>
</ul>
<a class="btn" href="#">Sign Up</a>
</div></div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 " >
<div class="pricetable active">
<div class="title uppercase">Premium</div>
<div class="clearfix"></div>
<div class="price-box">
<div class="price">$39.99<span>/Month</span></div>
</div>
<ul class="plan_features">
<li>1 GB Bandwidth</li>
<li>256 MB Memory</li>
<li>Unlimited</li>
<li>Full Support</li>
<li>Free Domain</li>
</ul>
<a class="btn" href="#">Sign Up</a>
</div></div>

<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 " >
<div class="pricetable">
<div class="title uppercase">Unlimited</div>
<div class="clearfix"></div>
<div class="price-box">
<div class="price">$49.99<span>/Month</span></div>
</div>
<ul class="plan_features">
<li>1 GB Bandwidth</li>
<li>256 MB Memory</li>
<li>Unlimited</li>
<li>Full Support</li>
<li>Free Domain</li>
</ul>
<a class="btn" href="#">Sign Up</a>
</div></div>

</div>
</div></div></section>

<div class="clearfix"></div>
<?php get_footer(); ?>